<?php

$config['sources'] = [
	'default' => [
		'root' => __DIR__ . '/images',
		'baseurl' => 'images/',
		'maxFileSize' => '10000kb',
		'createThumb' => true,
		'extensions' => ['jpg'],
	]
];
return $config;