_app_config = {
	inlinesearch:true,
    suggestionFlag:false,
    suggestionString:"",
    shortCutKeys:[],    
    MQ : MathQuill.getInterface(2)
}

window.addEventListener("load", () => {           
    var editorObj = (function () {
        joditInstance =  {};
        return {
            'set': function (jodit) {
                joditInstance = jodit
            },
            'write': function (latex_key) {
                if(joditInstance.value == undefined){}
                var latex = getLatexCode(latex_key);
                var randomId = getRandomId();
               // joditInstance.value += "<div id='" + randomId + "'></div><span>&nbsp;</span>";
                var mathApply = joditInstance.mathquillNesting.insertLatex(randomId, latex);
                console.log(mathApply);
                if(mathApply){
                    var katexSpan = document.getElementById(randomId);
                    // console.log(katexSpan);
                    if(latex && latex.latex){
                        applyMathquill(randomId, latex.latex);
                        // katex.render(latex.latex, document.querySelector(".math-span:last-child"), {
                        //     throwOnError: false
                        // });
                    }
                }
            }
        }
    })();

    var editor = new Jodit('#editor', {
        textIcons: false,
        iframe: false,
        iframeStyle: '*,.jodit_wysiwyg {color:red;}',
        height: 300,
        defaultMode: Jodit.MODE_WYSIWYG,
        removeButtons: ['fullsize','selectall','print','about','symbol'],
        observer: {
            timeout: 100
        },
        uploader: {
            url: 'index.php?action=fileUpload'
        },
        filebrowser: {
            buttons: ['list', 'tiles', 'sort'],
            ajax: {
                url: 'index.php'
            }
        },
        commandToHotkeys: {
            'openreplacedialog': 'ctrl+p'
        },
        
		extraButtons: [
            {
                name: 'Math functions',
                iconURL: 'icons/math_icon_small.png',
                tooltip: 'Math Functions',
                exec: function () {
                    showMathToolBar(editor);
                }
            }
        ],
        events: {
            afterInit: function (editor) {
                editor.mathquillNesting = new Jodit.modules.MathquillNesting(editor);
            }
        }
    });    
    editorObj.set(editor);
    editor.events.on('change', (ev)=>{
        //console.log(ev.keyCode);
        //console.log(editor.selection.current());
    });
    editor.events.on('keypress', (ev)=>{
        const _char_code = ev.which || ev.keyCode;
        if(_app_config.inlinesearch==true){
            if(_char_code===47){
                if(!document.querySelector("#inlineMathSearch")){ 
                _app_config.suggestionFlag = true;
                let searchInput = Jodit.modules.Dom.create('span', '', editor.ownerDocument);
                searchInput.id = 'inlineMathSearch';
                editor.selection.insertNode(searchInput);
                }
            }
            if(_char_code==27 || _char_code==13 || _char_code==9 || _char_code==32){
                removeAutoComplete();
                if(document.querySelector("#inlineMathSearch")){
                    document.querySelector("#inlineMathSearch").remove();
                }
                _app_config.suggestionFlag = false;
                _app_config.suggestionString ="";
            }


            if(_app_config.suggestionFlag == true){
                if(_char_code!=47){_app_config.suggestionString += ev.key ;}
                getMathAutoComplete(editor);
            } 
        }        
            
        
    });
    editor.events.on('keyup', (ev)=>{
        const _char_code = ev.key || ev.keyCode;

        _app_config.shortCutKeys.push(_char_code) ;
       // console.log(_app_config.shortCutKeys);
        //let found = arr1.some(r=> arr2.indexOf(r) >= 0)
        if(_app_config.shortCutKeys[0] == 191 &&  _app_config.shortCutKeys[1] == 16 && _app_config.shortCutKeys[2] == 17){
            // cntr + shift + ?
            editorObj.write("fraction");
        }
        if(_app_config.shortCutKeys[0] == 190 &&  _app_config.shortCutKeys[1] == 16 && _app_config.shortCutKeys[2] == 17){
            // cntr + shift + >
            editorObj.write("cube_root");
        }
        if(_app_config.shortCutKeys[0] == 188 &&  _app_config.shortCutKeys[1] == 16 && _app_config.shortCutKeys[2] == 17){
            // cntr + shift + <
            editorObj.write("square_root");
        }
    });
    editor.events.on('keydown', (ev)=>{
        const _char_code = ev.which || ev.keyCode; 
       _app_config.shortCutKeys=[];
        if(_char_code == 9){
            ev.preventDefault();
                if(_app_config.suggestionString != ""){
                    let _filtered_array =_expression_list.filter(function(itm){
                        return (itm.expression == _app_config.suggestionString);
                });
                console.log(_filtered_array);
                if(_filtered_array.length > 0){
                    editorObj.write(_filtered_array[0]["expression"]);
                    replaceSlashFragment(joditInstance.editor.childNodes);
                    removeAutoComplete();
                }
            }
        } 
    });
function removeAutoComplete(){
    if(document.querySelector(".autocomplete-container")){
        document.querySelector(".autocomplete-container").remove();
    }
}    

function getMathAutoComplete(editor){
    removeAutoComplete();
    let _filtered_array =_expression_list.filter(function(itm){
            return itm.name.toLowerCase().indexOf(_app_config.suggestionString.toLowerCase()) > -1;
    });
    if(_filtered_array.length > 0){
        let _autocomplete_container = document.createElement("div");
        _autocomplete_container.classList.add("autocomplete-container");
        let _search_offset = offset(document.querySelector("#inlineMathSearch"));
        _autocomplete_container.style.top =(_search_offset.top + 5) +"px";
        _autocomplete_container.style.left = (_search_offset.left - 8) + "px";
        document.querySelector(".wrapper").appendChild(_autocomplete_container);
        _autocomplete_container.innerHTML=`<div class="autocomplete-top"><i>Command matching "${_app_config.suggestionString}"</i></div><div class="autocomplete-main"><ul></ul></div>`;            
        let _autocomplete_list = document.querySelector(".autocomplete-container .autocomplete-main ul");
        
        _filtered_array.map((obj) => {
            try{ 
            let _exp = getLatexCode(obj.expression).latex || "";
            let _autocomplete_item = document.createElement("li");
            _autocomplete_item.innerHTML = `<a href="javascript:void(0)" data-expression="${obj.expression}">${obj.name}<b>${_exp}</b></a>`
            _autocomplete_list.appendChild(_autocomplete_item);
           // _app_config.MQ.StaticMath(_autocomplete_item.querySelector("a > b"));
            }catch(exp){
                //console.log(exp)
            }
            finally{}
    });
    attachMathListEvent(document.querySelector('.autocomplete-container'));
   }
}

function showMathToolBar(editor) {
    if(document.querySelector(".math-toolbar")){
        document.querySelector(".math-toolbar").remove();
        return;
    }
    let _math_toolbar = document.createElement("div");
    _math_toolbar.classList.add("math-toolbar");
    document.querySelector(".wrapper").appendChild(_math_toolbar);

    _math_toolbar.innerHTML=`<div class="math-toolbar-top">
	     <div class="float-left">
			<h3>Maths & Symbols</h3>
			</div>
			<div class="float-right utilities">
			<a href="javascript:void(0)" id="math_toolbar_search"><i class="fas fa-search"></i><a>
			<a href="javascript:void(0)" id="math_toolbar_close"><i class="fas fa-times"></i></a>
			</div>
			<div class="clear"></div> </div>
			<div class="search-container hide"><input type="text" placeholder="Search Expression" value=""/></div>
			<div class="math-toolbar-main">
			<div class="group-container"></div><div class="group-items-container"><ul></ul></div>
			</div>`;    

    let _group_list = ["Group1", "Group2", "Group3", "Group4", "Group5", "Group6", "Group7", "Group8"];
    
    let _math_group_list = document.createElement("ul");
    document.querySelector("div.group-container").appendChild(_math_group_list);

    _group_list.map((obj) => {
        let _group_container = document.createElement("li");
        _group_container.innerHTML = `<a href="javascript:void(0);">${obj}</a> `;
        _math_group_list.appendChild(_group_container);
    });
   
    document.querySelectorAll(".math-toolbar .math-toolbar-main .group-container ul li a").forEach((elem)=>{
        elem.addEventListener("click",(ev)=>{
            let _list_container = document.querySelector("div.group-items-container");
            _list_container.innerHTML="";
            let _list_items = document.createElement("ul");
            _list_container.appendChild(_list_items);
             _expression_list.map((obj) => {
				try{ 
					 let _exp = getLatexCode(obj.expression).latex || "";
					 let _math_icon = document.createElement("li");
					 _math_icon.innerHTML = `<a href="javascript:void(0)" class="math-expression" data-expression="${obj.expression}">${_exp}</a>`;
					 _list_items.appendChild(_math_icon);			 
					// _app_config.MQ.StaticMath(_math_icon.querySelector("a"));
			  
				}catch(exp){
					//console.log(exp);
				}finally{
					//continue;
				}
             });
             attachMathListEvent(document.querySelector('.math-toolbar'));
          });
    });
   document.querySelector(".math-toolbar .math-toolbar-main .group-container ul li a:first-child").click();

   
    document.querySelector("#math_toolbar_close").addEventListener("click",(ev)=>{
        ev.preventDefault();
        document.querySelector('.math-toolbar').remove();
    });
	
    document.querySelector("#math_toolbar_search").addEventListener("click",(ev)=>{
        ev.preventDefault();
        document.querySelector('.math-toolbar .search-container').classList.toggle("hide");
    });
	
	
  }

  function attachMathListEvent(containerObj){
    let _math_list_elem_arr = containerObj.querySelectorAll("ul > li > a");
    if (containerObj.classList.contains('math-toolbar')) {
        _math_list_elem_arr = containerObj.querySelectorAll(".group-items-container ul > li > a");
    }
    _math_list_elem_arr.forEach((obj) => {
        obj.addEventListener("click", (event) => {
        let _exp = obj.getAttribute("data-expression");
        editorObj.write(_exp);
        containerObj.remove();
        if(document.querySelector("#inlineMathSearch")){
            document.querySelector("#inlineMathSearch").remove();
        }
        replaceSlashFragment(joditInstance.editor.childNodes);
        
      });
    });
  }

  function offset(el) {
    var rect = el.getBoundingClientRect(),
    scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
    scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
  }
  function replaceSlashFragment(nodesArray){
    if(nodesArray.length > 0){
        nodesArray.forEach((obj, idx)=>{
            //if(!obj.id && obj.nodeValue== ("/"+_app_config.suggestionString)){obj.remove();}
            if(obj.nodeName=="DIV" && obj.classList.contains("latex-element-block")){
                var _curr_str = "/"+_app_config.suggestionString;
                if(obj.previousSibling && obj.previousSibling.nodeName=="#text" && obj.previousSibling.nodeValue.indexOf(_curr_str) != -1){
                    obj.previousSibling.nodeValue = obj.previousSibling.nodeValue.replace(_curr_str,"");
                }
            }            
            if(obj.childNodes.length > 0){replaceSlashFragment(obj.childNodes)}
        });   
    }
  }
})
