var button_meta = {
    "fraction": {latex: "\\frac{ }{ }", moveto: "Up", movefor: 1, tab: 1, icon: '\\frac{\\square}{\\square}'},
    "mix_fraction": {latex: "\\binom{ }{ }", moveto: "Up", movefor: 1, tab: 1, icon: '\\frac{\\square}{\\square}'},
    "square_root": {latex: "\\sqrt{ }", moveto: "Left", movefor: 1, tab: 1, icon: '\\sqrt{\\square}'},
    "cube_root": {latex: "\\sqrt[3]{ }", moveto: "Left", movefor: 1, tab: 1, icon: '\\sqrt[3]{\\square}'},
    "root": {latex: "\\sqrt[{ }]{ }", moveto: "Left", movefor: 2, tab: 1, icon: '\\sqrt[\\square]{\\square}'},
    "integration": {latex: "\\int", tab: 3, icon: '\\int'},
    "superscript": {latex: "\\^{ }", moveto: "Up", movefor: 1, tab: 1, icon: '\\square^2'},
    "subscript": {latex: "\\_{ }", moveto: "Down", movefor: 1, tab: 1, icon: '\\square_{2}'},
    "multiplication": {latex: "\\times", tab: 2, icon: '\\times'},
    "division": {latex: "\\div", tab: 2, icon: '\\div'},
    "plus_minus": {latex: "\\pm", tab: 2, icon: '\\pm'},
    "pi": {latex: "\\pi", tab: 2, icon: '\\pi'},
    "degree": {latex: "\\degree", tab: 2, icon: '\\degree'},
    "not_equal": {latex: "\\neq", tab: 2, icon: '\\neq'},
    "greater_equal": {latex: "\\geq", tab: 2, icon: '\\geq'},
    "less_equal": {latex: "\\leq", tab: 2, icon: '\\leq'},
    "greater_than": {latex: "\\gt", tab: 2, icon: '\\gt'},
    "less_than": {latex: "\\lt", tab: 2, icon: '\\lt'},
    "angle": {latex: "\\angle", tab: 3, icon: '\\angle'},
    "parallel_to": {latex: "\\parallel", tab: 3, icon: '\\parallel'},
    "perpendicular": {latex: "\\perpendicular", tab: 3, icon: '\\perpendicular'},
    "triangle": {latex: "\\triangle", tab: 3, icon: '\\triangle'},
    "parallelogram": {latex: "\\parallelogram", tab: 3, icon: '\\parallelogram'},
    "round_brackets": {latex: "\\left(\\right)", moveto: "Left", movefor: 1, tab: 1, icon: '\\left(\\square\\right)'},
    "alpha": {latex: "\\alpha", tab: 3, icon: '\\alpha'},
    "beta": {latex: "\\beta", tab: 3, icon: '\\beta'},
    "chi": {latex: "\\chi", tab: 3, icon: '\\chi'},
    "delta": {latex: "\\delta", tab: 3, icon: '\\delta'},
    "epsilon": {latex: "\\epsilon", tab: 3, icon: '\\epsilon'},
    "phi-var": {latex: "\\varPhi", tab: 3, icon: '\\phi-var'},
    "phi": {latex: "\\phi", tab: 3, icon: '\\phi'},
    "gamma": {latex: "\\gamma", tab: 3, icon: '\\gamma'},
    "eta": {latex: "\\eta", tab: 3, icon: '\\eta'},
    "iota": {latex: "\\iota", tab: 3, icon: '\\iota'},
    "kappa": {latex: "\\kappa", tab: 3, icon: '\\kappa'},
    "lambda": {latex: "\\lambda", tab: 3, icon: '\\lambda'},
    "mu": {latex: "\\mu", tab: 3, icon: '\\mu'},
    "nu": {latex: "\\nu", tab: 3, icon: '\\nu'},
    "omicron": {latex: "\\omicron", tab: 3, icon: '\\omicron'},
    "pi": {latex: "\\pi", tab: 3, icon: '\\pi'},
    "pi-var": {latex: "\\varpi", tab: 3, icon: '\\pi-var'},
    "theta": {latex: "\\theta", tab: 3, icon: '\\theta'},
    "theta-var": {latex: "\\varTheta", tab: 3, icon: '\\theta-var'},
    "rho": {latex: "\\rho", tab: 3, icon: '\\rho'},
    "sigma": {latex: "\\sigma", tab: 3, icon: '\\sigma'},
    "sigma-final": {latex: "\\varsigma", tab: 3, icon: '\\sigma-final'},
    "tau": {latex: "\\tau", tab: 3, icon: '\\tau'},
    "upsilon": {latex: "\\upsilon", tab: 3, icon: '\\upsilon'},
    "omega": {latex: "\\omega", tab: 3, icon: '\\omega'},
    "xi": {latex: "\\xi", tab: 3, icon: '\\xi'},
    "psi": {latex: "\\psi", tab: 3, icon: '\\psi'},
    "zeta": {latex: "\\zeta", tab: 3, icon: '\\zeta'},
    "omega-big": {latex: "\\Omega", tab: 3, icon: '\\omega-big'},
    "xi-big": {latex: "\\varXi", tab: 3, icon: '\\xi-big'},
    "psi-big": {latex: "\\\varPsi", tab: 3, icon: '\\psi-big'},


    "not": {latex: "\\not", tab: 3, icon: '\\not'},
    "complement": {latex: "\\complement", tab: 3, icon: '\\complement'},

    "notexists": {latex: "\\nexists", tab: 3, icon: '\\notexists'},
    "circle-big": {latex: "\\bigcirc", tab: 3, icon: '\\circle-big'},
    "ominus": {latex: "\\ominus", tab: 3, icon: '\\ominus'},
    "aster": {latex: "*", tab: 3, icon: '\\aster'},
    "mid": {latex: "\\mid", tab: 3, icon: '\\mid'},
    "parallel": {latex: "\\parallel", tab: 3, icon: '\\parallel'},
    "subset-eq": {latex: "\\subset", tab: 3, icon: '\\subset-eq'},

    "rho-var": {latex: "\\varrho", tab: 3, icon: '\\rho-var'},
    "gamma-big": {latex: "\\Gamma", tab: 3, icon: '\\gamma-big'},
    "theta-big": {latex: "\\Theta", tab: 3, icon: '\\theta-big'},
    "lambda-big": {latex: "\\Lambda", tab: 3, icon: '\\lambda-big'},
    "pi-big": {latex: "\\Pi", tab: 3, icon: '\\pi-big'},
    "sigma-big": {latex: "\\Sigma", tab: 3, icon: '\\sigma-big'},
    "upsilon-big": {latex: "\\Upsilon", tab: 3, icon: '\\upsilon-big'},
    "phi-big": {latex: "\\Phi", tab: 3, icon: '\\phi-big'},
    "sin": {latex: "\\sin", tab: 3, icon: '\\sin'},
    "cos": {latex: "\\cos", tab: 3, icon: '\\cos'},
    "tan": {latex: "\\tan", tab: 3, icon: '\\tan'},
    "sec": {latex: "\\sec", tab: 3, icon: '\\sec'},
    "cosec": {latex: "\\cosec", tab: 3, icon: '\\cosec'},
    "cot": {latex: "\\cot", tab: 3, icon: '\\cot'},
    "diff": {latex: "\\diff", tab: 3, icon: '\\diff'},
    "lim": {latex: "\\lim", tab: 3, icon: '\\lim'},
    "log": {latex: "\\log", tab: 3, icon: '\\log'},
    "min": {latex: "\\min", tab: 3, icon: '\\min'},
    "max": {latex: "\\max", tab: 3, icon: '\\max'},

    "quad": {latex: "\\quad", tab: 3, icon: '\\quad'},
    "qquad": {latex: "\\qquad", tab: 3, icon: '\\qquad'},
    "overleftarrow_tb": {latex: "\\overleftarrow{}", tab: 3, icon: '\\overleftarrow{}'},
    "overleftarrow_l": {latex: "\\overleftarrow{AB}", tab: 3, icon: '\\overleftarrow{AB}'},
    "overrightarrow_r": {latex: "\\overrightarrow{}", tab: 3, icon: '\\overleftarrow{AB}'},
    "underrightarrow_ru": {latex: "\\underrightarrow{}", tab: 3, icon: '\\overleftarrow{AB}'},
    "overleftrightarrow_r": {latex: "\\underleftrightarrow{}", tab: 3, icon: '\\overleftarrow{}'},
    "overleftrightarrow_lr": {latex: "\\overleftrightarrow{}", tab: 3, icon: '\\overleftarrow{}'},

};

function getLatexCode(latex_key) {
    return button_meta[latex_key];
}

function applyMathquill_(latex, id) {
    answerSpan = document.querySelector(".math-span:last-child")
    // answerSpan = document.getElementById('test');
    MQ = MathQuill.getInterface(2);
    answerMathField = MQ.MathField(answerSpan, {});
    answerMathField.write('\\sqrt{2}');
    answerMathField.focus();
    answerMathField.keystroke(button_meta[option_name].moveto);
}

function getRandomId() {
    var date = new Date();
    return 'T' + date.getTime();
}

function applyMathquill(id, latex) {
    var MQ = MathQuill.getInterface(2);
    var answerSpan = document.getElementById(id);
    var answerMathField = MQ.MathField(answerSpan, {});
    answerMathField.write(latex);
    answerMathField.focus();
    mathQuillInstances[id] = answerMathField;
}

function setEditorLatex() {
    $('div.latex-element-block').map((index, element) => {
        var divId = $(element).attr('id');
        var latexValue = mathQuillInstances[divId].latex();
        $(element).attr('data-latex', latexValue);
    });
    console.log('editor output', editor.innerHTML);
}