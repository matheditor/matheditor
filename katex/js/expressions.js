const _group_list = ["Group1", "Group2", "Group3", "Group4", "Group5", "Group6", "Group7", "Group8"];
const _expression_list = [{
    name: 'Fraction',
    iconURL: 'icons/sqrt_icon_small.png',
    tooltip: 'Square Root',
    expression: 'fraction',
    shortcutkeys:['']
}, {
    name: 'Binomial',
    iconURL: 'icons/sqrt_icon_small.png',
    tooltip: 'Plus Minus',
    expression: 'mix_fraction'
}, {
    name: 'square_root',
    iconURL: 'icons/sqrt_icon_small.png',
    tooltip: 'square_root',
    expression: 'square_root'
}, {
    name: 'oleftarr',
    iconURL: 'icons/diff.png',
    tooltip: 'overleftarrow_tb',
    expression: 'overleftarrow_tb'
},{
    name: 'orightarr',
    iconURL: 'icons/diff.png',
    tooltip: 'diff',
    expression: 'overrightarrow_r'
}, {
    name: 'cube_root',
     iconURL: 'icons/sqrt_icon_small.png',
     tooltip: 'cube_root',
     expression: 'cube_root'
 }, {
     name: 'root',
     iconURL: 'icons/sqrt_icon_small.png',
     tooltip: 'root',
     expression: 'root'
 }, {
     name: 'integration',
     iconURL: 'icons/sqrt_icon_small.png',
     tooltip: 'integration',
     expression: 'integration'
 }, {
    name: 'multiplication',
    iconURL: 'icons/sqrt_icon_small.png',
    tooltip: 'multiplication',
    expression: 'multiplication'
}, {
    name: 'division',
    iconURL: 'icons/sqrt_icon_small.png',
    tooltip: 'division',
    expression: 'division'
}, {
    name: 'Plus Minus',
    iconURL: 'icons/sqrt_icon_small.png',
    tooltip: 'Plus Minus',
    expression: 'plus_minus'
}, {
    name: 'pi',
    iconURL: 'icons/sqrt_icon_small.png',
    tooltip: 'pi',
    expression: 'pi'
}, {
    name: 'degree',
    iconURL: 'icons/sqrt_icon_small.png',
    tooltip: 'degree',
    expression: 'degree'
}, {
    name: 'not_equal',
    iconURL: 'icons/sqrt_icon_small.png',
    tooltip: 'not_equal',
    expression: 'not_equal'
}, {
    name: 'greater_equal',
    iconURL: 'icons/sqrt_icon_small.png',
    tooltip: 'greater_equal',
    expression: 'greater_equal'
}, {
    name: 'less_equal',
    iconURL: 'icons/sqrt_icon_small.png',
    tooltip: 'less_equal',
    expression: 'less_equal'
}, {
    name: 'greater_than',
    iconURL: 'icons/sqrt_icon_small.png',
    tooltip: 'greater_than',
    expression: 'greater_than'
}, {
    name: 'less_than',
    iconURL: 'icons/sqrt_icon_small.png',
    tooltip: 'less_than',
    expression: 'less_than'
},  {
    name: 'angle',
    iconURL: 'icons/sqrt_icon_small.png',
    tooltip: 'angle',
    expression: 'angle'
}, {
    name: 'parallel_to',
    iconURL: 'icons/sqrt_icon_small.png',
    tooltip: 'Plus Minus',
    expression: 'parallel_to'
}, {
    name: 'Plus Minus',
    iconURL: 'icons/sqrt_icon_small.png',
    tooltip: 'Plus Minus',
    expression: 'perpendicular'
}, {
    name: 'triangle',
    iconURL: 'icons/sqrt_icon_small.png',
    tooltip: 'triangle',
    expression: 'triangle'
}, {
    name: 'parallelogram',
    iconURL: 'icons/sqrt_icon_small.png',
    tooltip: 'parallelogram',
    expression: 'parallelogram'
}, {
    name: 'round_brackets',
    iconURL: 'icons/sqrt_icon_small.png',
    tooltip: 'round_brackets',
    expression: 'round_brackets'
}, {
    name: 'alpha',
    iconURL: 'icons/alpha.png',
    tooltip: 'alpha',
    expression: 'alpha'
}, {
    name: 'beta',
    iconURL: 'icons/beta.png',
    tooltip: 'beta',
    expression: 'beta'
},
{
    name: 'delta',
    iconURL: 'icons/delta.png',
    tooltip: 'delta',
    expression: 'delta'
},{
    name: 'epsilon',
    iconURL: 'icons/epsilon.png',
    tooltip: 'epsilon',
    expression: 'epsilon'
},{
    name: 'phi var',
    iconURL: 'icons/phi var.png',
    tooltip: 'phi var',
    expression: 'phi var'
},{
    name: 'lambda',
    iconURL: 'icons/lambda.png',
    tooltip: 'lambda',
    expression: 'lambda'
},{
    name: 'kappa',
    iconURL: 'icons/kappa.png',
    tooltip: 'kappa',
    expression: 'kappa'
},{
    name: 'iota',
    iconURL: 'icons/iota.png',
    tooltip: 'iota',
    expression: 'iota'
},{
    name: 'eta',
    iconURL: 'icons/eta.png',
    tooltip: 'eta',
    expression: 'eta'
},{
    name: 'gamma',
    iconURL: 'icons/chi.png',
    tooltip: 'gamma',
    expression: 'gamma'
},{
    name: 'phi',
    iconURL: 'icons/chi.png',
    tooltip: 'phi',
    expression: 'phi'
},{
    name: 'theta var',
    iconURL: 'icons/theta var.png',
    tooltip: 'theta var',
    expression: 'theta var'
},{
    name: 'theta',
    iconURL: 'icons/theta.png',
    tooltip: 'theta',
    expression: 'theta'
},{
    name: 'pi var',
    iconURL: 'icons/pi var.png',
    tooltip: 'pi var',
    expression: 'pi var'
},{
    name: 'omicron',
    iconURL: 'icons/omicron.png',
    tooltip: 'omicron',
    expression: 'omicron'
},{
    name: 'nu',
    iconURL: 'icons/nu.png',
    tooltip: 'nu',
    expression: 'nu'
},{
    name: 'mu',
    iconURL: 'icons/mu.png',
    tooltip: 'mu',
    expression: 'mu'
},{
    name: 'aster',
    iconURL: 'icons/aster.png',
    tooltip: 'aster',
    expression: 'aster'
},{
    name: 'mid',
    iconURL: 'icons/mid.png',
    tooltip: 'mid',
    expression: 'mid'
},{
    name: 'parallel',
    iconURL: 'icons/parallel.png',
    tooltip: 'parallel',
    expression: 'parallel'
}, {
    name: 'subset eq',
    iconURL: 'icons/subset eq.png',
    tooltip: 'subset eq',
    expression: 'subset eq'
},{
    name: 'ominus',
    iconURL: 'icons/ominus.png',
    tooltip: 'ominus',
    expression: 'ominus'
},{
    name: 'circle big',
    iconURL: 'icons/circle big.png',
    tooltip: 'circle big',
    expression: 'circle big'
},{
    name: 'notexists',
    iconURL: 'icons/notexists.png',
    tooltip: 'notexists',
    expression: 'notexists'
},{
    name: 'complement',
    iconURL: 'icons/complement.png',
    tooltip: 'complement',
    expression: 'complement'
},{
    name: 'not',
    iconURL: 'icons/not.png',
    tooltip: 'not',
    expression: 'not'
},{
    name: 'psi big',
    iconURL: 'icons/psi big.png',
    tooltip: 'psi big',
    expression: 'psi big'
},{
    name: 'xi big',
    iconURL: 'icons/xi big.png',
    tooltip: 'xi big',
    expression: 'xi big'
}, {
    name: 'omega big',
    iconURL: 'icons/omega big.png',
    tooltip: 'omega big',
    expression: 'omega big'
},{
    name: 'zeta',
    iconURL: 'icons/zeta.png',
    tooltip: 'zeta',
    expression: 'zeta'
},{
    name: 'psi',
    iconURL: 'icons/psi.png',
    tooltip: 'psi',
    expression: 'psi'
},{
    name: 'xi',
    iconURL: 'icons/xi.png',
    tooltip: 'xi',
    expression: 'xi'
},{
    name: 'omega',
    iconURL: 'icons/omega.png',
    tooltip: 'omega',
    expression: 'omega'
}, {
    name: 'upsilon',
    iconURL: 'icons/upsilon.png',
    tooltip: 'upsilon',
    expression: 'upsilon'
},{
    name: 'tau',
    iconURL: 'icons/tau.png',
    tooltip: 'tau',
    expression: 'tau'
},{
    name: 'sigma final',
    iconURL: 'icons/sigma final.png',
    tooltip: 'sigma final',
    expression: 'sigma final'
},{
    name: 'sigma',
    iconURL: 'icons/sigma.png',
    tooltip: 'sigma',
    expression: 'sigma'
},{
    name: 'rho',
    iconURL: 'icons/rho.png',
    tooltip: 'rho',
    expression: 'rho'
},{
    name: 'rho var',
    iconURL: 'icons/rho var.png',
    tooltip: 'rho var',
    expression: 'rho var'
},{
    name: 'gamma big',
    iconURL: 'icons/gamma big.png',
    tooltip: 'gamma big',
    expression: 'gamma big'
},{
    name: 'theta big',
    iconURL: 'icons/theta big.png',
    tooltip: 'theta big',
    expression: 'theta big'
},{
    name: 'pi big',
    iconURL: 'icons/pi big.png',
    tooltip: 'pi big',
    expression: 'pi big'
},{
    name: 'sigma big',
    iconURL: 'icons/sigma big.png',
    tooltip: 'sigma big',
    expression: 'sigma big'
},{
    name: 'upsilon big',
    iconURL: 'icons/upsilon big.png',
    tooltip: 'upsilon big',
    expression: 'upsilon big'
},{
    name: 'lambda big',
    iconURL: 'icons/lambda big.png',
    tooltip: 'lambda big',
    expression: 'lambda big'
},{
    name: 'phi big',
    iconURL: 'icons/phi big.png',
    tooltip: 'phi big',
    expression: 'phi big'
},{
    name: 'phi big',
    iconURL: 'icons/phi big.png',
    tooltip: 'phi big',
    expression: 'phi big'
},{
    name: 'sin',
    iconURL: 'icons/sin.png',
    tooltip: 'sin',
    expression: 'sin'
},{
    name: 'cos',
    iconURL: 'icons/cos.png',
    tooltip: 'cos',
    expression: 'cos'
},{
    name: 'tan',
    iconURL: 'icons/tan.png',
    tooltip: 'tan',
    expression: 'tan'
},{
    name: 'sec',
    iconURL: 'icons/sec.png',
    tooltip: 'sec',
    expression: 'sec'
},{
    name: 'cosec',
    iconURL: 'icons/cosec.png',
    tooltip: 'cosec',
    expression: 'cosec'
},{
    name: 'cot',
    iconURL: 'icons/chi.png',
    tooltip: 'cot',
    expression: 'cot'
},{
 name: 'qquad',
    iconURL: 'icons/qquad.png',
    tooltip: 'qquad',
    expression: 'qquad'
},{
    name: 'quad',
    iconURL: 'icons/quad.png',
    tooltip: 'quad',
    expression: 'quad'
},{
    name: 'max',
    iconURL: 'icons/max.png',
    tooltip: 'max',
    expression: 'max'
},{
    name: 'min',
    iconURL: 'icons/min.png',
    tooltip: 'min',
    expression: 'min'
},{
    name: 'log',
    iconURL: 'icons/log.png',
    tooltip: 'log',
    expression: 'log'
},{
    name: 'lim',
    iconURL: 'icons/lim.png',
    tooltip: 'lim',
    expression: 'lim'
},{
    name: 'diff',
    iconURL: 'icons/diff.png',
    tooltip: 'diff',
    expression: 'diff'
},{
    name: 'oleftarr',
    iconURL: 'icons/diff.png',
    tooltip: 'overleftarrow_tb',
    expression: 'overleftarrow_tb'
},{
    name: 'orightarr',
    iconURL: 'icons/diff.png',
    tooltip: 'diff',
    expression: 'overleftrightarrow_lr'
}
,{
    name: 'uleftright',
    iconURL: 'icons/diff.png',
    tooltip: 'diff',
    expression: 'underleftrightarrow_lu'
}
,{
    name: 'urightarr',
    iconURL: 'icons/diff.png',
    tooltip: 'diff',
    expression: 'underrightarrow_ru'
}
,{
    name: 'orightarr',
    iconURL: 'icons/diff.png',
    tooltip: 'diff',
    expression: 'overrightarrow_r'
}
,{
    name: 'oleftarr',
    iconURL: 'icons/diff.png',
    tooltip: 'diff',
    expression: 'overleftarrow_l'
}

];