var mathQuillInstances = {};
Jodit.modules.MathquillNesting = function (editor) {
    this.insertLatex = function (randomId, latex) {
        var latexDiv = Jodit.modules.Dom.create('div', '', editor.ownerDocument);
        latexDiv.setAttribute('id', randomId);
        latexDiv.setAttribute('class', 'latex-element-block');
        if(editor.selection.current().className == 'mq-textarea'){
            var id = editor.selection.current().parentElement.id;
            mathQuillInstances[id].write(latex.latex);
            return false;
        } else {
            editor.selection.insertNode(latexDiv);
            editor.setEditorValue(); // for syncronize value between source textarea and editor
            return true;
        }

    };
};